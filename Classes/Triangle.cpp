#include "../Headers/Triangle.h"
using namespace std;
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include "../Headers/Edge.h"

Triangle::Triangle()
{

	Vertex a(0, 0, 0);
	Vertex b(0, 0, 0);
	Vertex c(0, 0, 0);

	p1 = a;
	p2 = b;
	p3 = c;

}

Triangle::~Triangle()
{
}

void Triangle::print_vertices()
{

	p1.print_Vertexs();
	p2.print_Vertexs();
	p3.print_Vertexs();

}

Vertex Triangle::getP1()
{
	return p1;
}

Vertex Triangle::getP2()
{
	return p2;
}

Vertex Triangle::getP3()
{
	return p3;
}

bool Triangle::same_Side(const Vertex& a, const Vertex& b, const Vertex& c)
{

	return (((b.returnX() - a.returnX())*(c.returnY() - a.returnY())) -
		((b.returnY() - a.returnY())*(c.returnX() - a.returnX()))) > 0;

}

bool Triangle::overlap(Triangle& b)
{

	std::vector<Edge> holder;

	const Vertex& pone = this->getP1();
	const Vertex& ptwo = this->getP2();
	const Vertex& pthree = this->getP3();
	const Vertex& p3 = b.getP1();
	const Vertex& p4 = b.getP2();
	const Vertex& p5 = b.getP3();

	const Edge e1(pone, ptwo, pthree);
	const Edge e2(ptwo, pthree, pone);
	const Edge e3(pone, pthree, ptwo);
	const Edge e4(p3, p4, p5);
	const Edge e5(p4, p5, p3);
	const Edge e6(p3, p5, p4);

	holder.push_back(e1);
	holder.push_back(e2);
	holder.push_back(e3);
	holder.push_back(e4);
	holder.push_back(e5);
	holder.push_back(e6);

	//create necessary edges and iterate through to determine if two triangles overlap (which share an edge)

	for (size_t u = 0; u < holder.size(); u++)
	{

		Edge& te1 = holder[u];

		for (size_t y = 0; y < holder.size(); y++)
		{

			Edge& te2 = holder[y];

			if (te1 == te2 && &te1 != &te2)
			{

				if (same_Side(te1.getP1(), te1.getP2(), te1.getNonEdgeVertex()) == same_Side(te1.getP1(), te1.getP2(), te2.getNonEdgeVertex()))
				{

					return true;

				}

			}

		}

	}

	return false;

}

int Triangle::intersect_Ray(vector<Vertex> &ray, Vertex &intersect)
{

	// Copyright 2001 softSurfer, 2012 Dan Sunday
	// This code may be freely used, distributed and modified for any purpose
	// providing that this copyright notice is included with it.
	// SoftSurfer makes no warranty for this code, and cannot be held
	// liable for any real or imagined damage resulting from its use.
	// Users of this code must verify correctness for their application.

	Vertex u, v, n;
	Vertex dir, w0, w;

	double r, a, b;

	Vertex empty(0, 0, 0);

	u = this->getP2() - this->getP1();
	v = this->getP3() - this->getP1();
	//u.print_Vertexs();
	//v.print_Vertexs();

	n = cross_Product(u,v);
	//n.print_Vertexs();

	if (n == empty)
	{
		//cout << "Incorrect triangle definition, stopping." << '\n';
		return -1;

	}

	dir = ray[1] - ray[0];
	w0 = ray[0] - this->getP1();
	
	a = -dot_Product(n, w0);

	b = dot_Product(n, dir);

	if (fabs(b) < 0.000001) // ray is parallel to the plane of the triangle
	{

		if (a == 0)
		{
			//cout << "Ray lies in the triangle plane, returning." << '\n';
			return 2;

		}
		else {
			//cout << "Ray disjoint from our plane, returning." << '\n';
			return 0;

		}

	}

	r = a / b;
	//cout << r << '\n';

	if (r < 0.0 || r > 1.0)
	{
		//cout << "No intersect - the ray faces away from the plane." << '\n';
		return 0;

	}

	intersect = ray[0] + dir*r;

	double uu, uv, vv, wu, wv, D;

	uu = dot_Product(u, u);
	uv = dot_Product(u, v);
	vv = dot_Product(v, v);
	w = intersect - this->getP1();
	wu = dot_Product(w, u);
	wv = dot_Product(w, v);
	D = uv * uv - uu * vv;

	double s, t;
	s = (uv * wv - vv * wu) / D;
	if (s < 0.0 || s > 1.0)
	{
		//cout << "Outside of the triangle." << '\n';
		return 0;

	}
	t = (uv * wu - uu * wv) / D;
	if (t < 0.0 || (s + t) > 1.0)
	{
		//cout << "Outside of the triangle." << '\n';
		return 0;

	}

	//cout << "Our point is inside the triangle, returning." << '\n';
	return 1;
}

Vertex Triangle::cross_Product(Vertex &a, Vertex &b)
{

	return Vertex(a.returnY()*b.returnZ() - a.returnZ()*b.returnY(), a.returnZ()*b.returnX() - a.returnX()*b.returnZ(), a.returnX()*b.returnY() - a.returnY()*b.returnX());

}

double Triangle::dot_Product(Vertex &a, Vertex &b)
{

	return a.returnX()*b.returnX() + a.returnY()*b.returnY() + a.returnZ()*b.returnZ();

}

pair<Vertex, Vertex> Triangle::surface_normal()
{

	Vertex u, v, n;

	u = this->getP2() - this->getP1();
	v = this->getP3() - this->getP1();

	n = cross_Product(u, v);

	const double mag = n.returnX() + n.returnY() + n.returnZ();

	n = n / mag;

	const double d = -dot_Product(n,this->getP1());
	Vertex constant(d, 0, 0);

	return make_pair(n,constant);

}