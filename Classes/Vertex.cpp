#include "../Headers/Vertex.h"
using namespace std;
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <limits>

Vertex::Vertex(double x1, double y1, double z1)
{

	x = x1;
	y = y1;
	z = z1;

}

Vertex::Vertex()
{

	x = 0;
	y = 0;
	z = 0;

}

Vertex::~Vertex()
{
}

void Vertex::print_Vertexs()
{


	cout.precision(6);
	cout << fixed << x << "," << fixed << y << "," << fixed << z << endl;

}

double Vertex::returnX() const
{
	return x;
}

double Vertex::returnY() const
{
	return y;
}

double Vertex::returnZ() const
{
	return z;
}