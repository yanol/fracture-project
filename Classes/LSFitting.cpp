#include "../Headers/LSFitting.h"
using namespace std;
#include <iostream>
#include <sstream>
#include <fstream>

LSFitting::LSFitting(std::vector<Vertex*> in)
{

	if (generate_Matrix(in))
	{

		if (invert_Matrix(matrix_1))
		{

			if (generate_Constants())
			{

				cout << "Successfully fitted a polynomial curve to the points." << '\n';
				cout << "Constants are: a_0 = " << a_0 << ", a_1 = " << a_1 << ", a_2 = " << a_2 << "." << '\n';

			}

		}

	}

}

LSFitting::~LSFitting()
{
}

bool LSFitting::generate_Matrix(std::vector<Vertex*> in)
{

	for (size_t i = 0; i < in.size(); i++)
	{

		Vertex& a = *in[i];

		double x = a.returnX();
		double y = a.returnY();
		
		sum_x += x;
		sum_x2 += x*x;
		sum_x3 += x*x*x;
		sum_x4 += x*x*x*x;

		sum_y += y;
		sum_xy += x*y;
		sum_x2y += (x*x) * y;

	}

	matrix_1.push_back(in.size());
	matrix_1.push_back(sum_x);
	matrix_1.push_back(sum_x2);
	matrix_1.push_back(sum_x);
	matrix_1.push_back(sum_x2);
	matrix_1.push_back(sum_x3);
	matrix_1.push_back(sum_x2);
	matrix_1.push_back(sum_x3);
	matrix_1.push_back(sum_x4);

	matrix_2.push_back(sum_y);
	matrix_2.push_back(sum_xy);
	matrix_2.push_back(sum_x2y);

	return matrix_1.size() == 9 && matrix_2.size() == 3;

}

bool LSFitting::invert_Matrix(std::vector<double> &in)
{

	std::vector<double> temp;

	double det_a = in[0] * (in[4] * in[8] - in[5] * in[7]) - 
		in[1]*(in[3]*in[8] - in[5]*in[6])
		+ in[2]*(in[3]*in[7] - in[4]*in[6]); 

	if (det_a > 0.0001)

	{

		double inv_det = 1 / det_a;

		temp.push_back(inv_det*(in[4] * in[8] - in[7] * in[5]));
		temp.push_back(inv_det*(in[2] * in[7] - in[8] * in[1]));
		temp.push_back(inv_det*(in[1] * in[5] - in[4] * in[2]));

		temp.push_back(inv_det*(in[5] * in[6] - in[8] * in[3]));
		temp.push_back(inv_det*(in[0] * in[8] - in[6] * in[2]));
		temp.push_back(inv_det*(in[2] * in[3] - in[5] * in[0]));

		temp.push_back(inv_det*(in[3] * in[7] - in[6] * in[4]));
		temp.push_back(inv_det*(in[1] * in[6] - in[7] * in[0]));
		temp.push_back(inv_det*(in[0] * in[4] - in[3] * in[1]));

		if (temp.size() == 9)
		{

			in = temp;
			return true;
		} return false;

	}
	else {

		return false;

	}

}

bool LSFitting::generate_Constants()
{
	if (matrix_1.size() == 9)
	{
		a_0 = (matrix_1[0] * matrix_2[0]) + (matrix_1[1] * matrix_2[1]) + (matrix_1[2] * matrix_2[2]);
		a_1 = (matrix_1[3] * matrix_2[0]) + (matrix_1[4] * matrix_2[1]) + (matrix_1[5] * matrix_2[2]);
		a_2 = (matrix_1[6] * matrix_2[0]) + (matrix_1[7] * matrix_2[1]) + (matrix_1[8] * matrix_2[2]);
		return true;
	}

	return false;

}