#include "../Headers/Mesh.h"
using namespace std;
#include <iostream>
#include <iterator>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include "../Headers/LSFitting.h"

Mesh::Mesh(const std::string inputfile, const string wall_name, const string fracture_name)
{

	if (create_Vertices_and_Edges(inputfile, wall_name, fracture_name))
	{

		cout << "Number of half edges is: " << hedges.size() << "." << '\n';
		cout << "Number of faces is: " << faces.size() << "." << '\n';
		//cout << "Initial point is: " << '\n';

	}
	else
	{

		cout << "Failed to generate half edge data structure for this mesh." << '\n';

	}
}

Mesh::~Mesh()
{
}

bool Mesh::create_Vertices_and_Edges(const string file, const string wall_name, const string fracture_name)
{

	string line;
	string p1 = "solid "+fracture_name;
	string p2 = "endsolid "+fracture_name;
	string p3 = "solid "+wall_name;
	string p4 = "endsolid "+wall_name;
	if (wall_name.empty())
		p3.clear(), p4.clear();

	ifstream Vertexs(file);

	std::vector<Vertex> temp_Vertexs;
	std::vector<Vertex> temp_Vertexs_rock;

	if (Vertexs.is_open())

	{

		// check to see if we should be reading the file yet, otherwise next line.
		size_t found = 0;
		size_t found_rock = 0;

			while (getline(Vertexs, line))

			{

				//vertices appear in lines of 3, so we wait until we have added 3, then process.
				if (temp_Vertexs.size() < 3 && temp_Vertexs_rock.size() < 3)

				{

					if (line.find(p1, 0) != string::npos)
					{

						found = 1;

					}
					if (line.find(p3, 0) != string::npos)
					{

						found_rock = 1;

					}
					if (line.find(p2, 0) != string::npos)
					{

						found = 2;

					}
					if (line.find(p4, 0) != string::npos)
					{

						found_rock = 2;

					}
					if (found == 1)
					{
						// we have found the start of the fracture, start reading.
						if (line.find("vertex", 0) != string::npos)
						{

							istringstream iss(line);

							std::vector<string> temp;

							std::copy(istream_iterator<string>(iss),
								istream_iterator<string>(),
								back_inserter(temp));

							const Vertex p1(std::stod(temp[1]), std::stod(temp[2]), std::stod(temp[3]));
							
							const int curr_index = vertices.size() + temp_Vertexs.size();
							//bounding box - find max in x direction == starting edge vertex.

							const int ip1(int(p1.returnX()));
							const int ia(int(first_edge_Vertex.returnX()));

							if (ip1 > ia)
							{
								
								first_edge_Vertex = p1;
								index_of_first_edge = curr_index;

							}
							else
							{

								if (ip1 == ia)
								{
									if (p1.returnX() > first_edge_Vertex.returnX())
									{

										first_edge_Vertex = p1;
										index_of_first_edge = curr_index;

									}

								}

							}

							temp_Vertexs.push_back(p1);

						}

					}
					if (found_rock == 1)
					{

						if (line.find("vertex", 0) != string::npos)
						{

							istringstream iss(line);

							std::vector<string> temp;

							std::copy(istream_iterator<string>(iss),
								istream_iterator<string>(),
								back_inserter(temp));

							const Vertex p1(std::stod(temp[1]), std::stod(temp[2]), std::stod(temp[3]));

							temp_Vertexs_rock.push_back(p1);

						}

					}
					else
					{
						//end of fracture.
						/*if (found == 2)
						{

							//cout << "Size of map is: " << half_edges.size() << "\n";
							return faces.size()*3 == hedges.size();

						}*/

					}
				}
				// else we have found our trio of vertices for the triangle.
				else

				{

					if (temp_Vertexs.size() == 3)
					{
						Vertex *p1 = new Vertex(temp_Vertexs[0]);
						Vertex *p2 = new Vertex(temp_Vertexs[1]);
						Vertex *p3 = new Vertex(temp_Vertexs[2]);

						Triangle *tempyt = new Triangle(*p1, *p2, *p3);

						//create map of half edge pairs

						Edge *e1 = new Edge(*p1, *p2, *p3);
						Edge *e2 = new Edge(*p2, *p3, *p1);
						Edge *e3 = new Edge(*p3, *p1, *p2);

						//create connections for the vertices
						p1->edge = e1;
						p2->edge = e2;
						p3->edge = e3;

						// store them

						faces.push_back(tempyt);

						hedges.push_back(e1);
						hedges.push_back(e2);
						hedges.push_back(e3);

						vertices.push_back(p1);
						vertices.push_back(p2);
						vertices.push_back(p3);

						//point each half edge face pointer to the triangle
						e1->adjacent_triangle = tempyt;
						e2->adjacent_triangle = tempyt;
						e3->adjacent_triangle = tempyt;

						//point each edge to the next edge in the triangle to create a loop
						e1->next = e2;
						e2->next = e3;
						e3->next = e1;

						//point each edge to the vertex it is pointing to
						e1->belongs_vertex = p2;
						e2->belongs_vertex = p3;
						e3->belongs_vertex = p1;

						// point the adjacent field of triangle tempyt to one of the half edges
						tempyt->edge = e1;

						std::pair<Vertex, Vertex> pair1 = std::make_pair(std::min(*p1, *p2), std::max(*p1, *p2));
						std::pair<Vertex, Vertex> pair2 = std::make_pair(std::min(*p2, *p3), std::max(*p2, *p3));
						std::pair<Vertex, Vertex> pair3 = std::make_pair(std::min(*p1, *p3), std::max(*p1, *p3));

						map< pair<Vertex, Vertex>, Edge* >::iterator find = half_edges.find(pair1);
						map< pair<Vertex, Vertex>, Edge* >::iterator find2 = half_edges.find(pair2);
						map< pair<Vertex, Vertex>, Edge* >::iterator find3 = half_edges.find(pair3);

						// find pairs of half edges (same edge, but bounded by different faces)
						if (find != half_edges.end())
						{

							Edge &y = *(find->second);
							e1->pair = &y;
							y.pair = e1;

						}
						else {

							half_edges.insert(std::make_pair(pair1, e1));

						}

						if (find2 != half_edges.end())
						{

							Edge &y = *(find2->second);
							e2->pair = &y;
							y.pair = e2;

						}
						else {

							half_edges.insert(std::make_pair(pair2, e2));

						}

						if (find3 != half_edges.end())
						{

							Edge &y = *(find3->second);
							e3->pair = &y;
							y.pair = e3;

						}
						else {

							half_edges.insert(std::make_pair(pair3, e3));

						}

						//half_edges.insert(std::make_pair(std::make_pair(std::min(e1.getP1(), e1.getP2()), std::max(e1.getP1(), e1.getP2())), e1));

						std::vector<Triangle> temp;
						temp.push_back(*tempyt);

						//clear temp so we can get the next set of 3 vertices.
						temp_Vertexs.clear();


					} 

					else {

						if (temp_Vertexs_rock.size() == 3)
						{

							Vertex *p1 = new Vertex(temp_Vertexs_rock[0]);
							Vertex *p2 = new Vertex(temp_Vertexs_rock[1]);
							Vertex *p3 = new Vertex(temp_Vertexs_rock[2]);

							Triangle *tempyt = new Triangle(*p1, *p2, *p3);

							//find the surface normal of the triangle in the rock AND the constant of the plane
							pair<Vertex, Vertex> surfnorm_constant = tempyt->surface_normal();

							//check if a triangle in the 'rock' exists with this surface normal and constant
							map <pair<Vertex, Vertex>, vector<Triangle*> >::iterator find = simple_rock_mesh.find(surfnorm_constant);
							vector<Triangle*> triangles; 
							triangles.push_back(tempyt);
							
							//if we find a triangle with the same surface normal, add it into the vector
							if (find != simple_rock_mesh.end())
							{

								vector<Triangle*> &hello = find->second;
								hello.push_back(tempyt);

							}
							else
							{

								simple_rock_mesh.insert(std::make_pair(surfnorm_constant, triangles));

							}

							rock.push_back(tempyt);

							temp_Vertexs_rock.clear();

						}

					}

				}
			}
		} 
	
		for (map <pair<Vertex,Vertex>, vector<Triangle*> >::iterator it = simple_rock_mesh.begin(); it != simple_rock_mesh.end(); ++it)
		{

			cout << it->second.size() << '\n';

		}
		cout << "Size of rock mesh simplified is: " << simple_rock_mesh.size() << "." << '\n';
		return true;
	}

void Mesh::vertices_into_File()
{

	ofstream xcoords("xcoords.txt");
	ofstream ycoords("ycoords.txt");
	ofstream zcoords("zcoords.txt");

	//edge_pointers.insert(edge_pointers.end(), current_extended_nodes.begin(), current_extended_nodes.end());

	for (size_t i = 0; i < all_edges_created.size(); i++)
	{

		Vertex &t = *all_edges_created[i].first;

		if (i != all_edges_created.size() - 1)

		{

			xcoords << t.returnX();
			xcoords << ",";

			ycoords << t.returnY();
			ycoords << ",";

			zcoords << t.returnZ();
			zcoords << ",";
		}

		else

		{

			xcoords << t.returnX();
			ycoords << t.returnY();
			zcoords << t.returnZ();

		}

	}

}

void Mesh::create_rhino_File(const string fileName)
{

	ofstream rhino(fileName);

	rhino << "solid FRACTURE" << '\n';

	for (size_t i = 0; i < faces.size(); i++)
	{

		rhino << "  facet normal 0.000000 0.000000 0.000000" << '\n';
		rhino << "    outer loop" << '\n';
		rhino << "      vertex " << faces[i]->getP1().returnX() << " " << faces[i]->getP1().returnY() << " " << faces[i]->getP1().returnZ() << '\n';
		rhino << "      vertex " << faces[i]->getP2().returnX() << " " << faces[i]->getP2().returnY() << " " << faces[i]->getP2().returnZ() << '\n';
		rhino << "      vertex " << faces[i]->getP3().returnX() << " " << faces[i]->getP3().returnY() << " " << faces[i]->getP3().returnZ() << '\n';
		rhino << "    endloop" << '\n';
		rhino << "  end facet" << '\n';

	}

	rhino << "endsolid FRACTURE" << '\n';

	rhino << "solid ROCK" << '\n';

	for (size_t i = 0; i < rock.size(); i++)
	{

		rhino << "  facet normal 0.000000 0.000000 0.000000" << '\n';
		rhino << "    outer loop" << '\n';
		rhino << "      vertex " << rock[i]->getP1().returnX() << " " << rock[i]->getP1().returnY() << " " << rock[i]->getP1().returnZ() << '\n';
		rhino << "      vertex " << rock[i]->getP2().returnX() << " " << rock[i]->getP2().returnY() << " " << rock[i]->getP2().returnZ() << '\n';
		rhino << "      vertex " << rock[i]->getP3().returnX() << " " << rock[i]->getP3().returnY() << " " << rock[i]->getP3().returnZ() << '\n';
		rhino << "    endloop" << '\n';
		rhino << "  end facet" << '\n';

	}

	rhino << "endsolid ROCK";

}

void Mesh::find_Edges_Pointers()	
{

	//totals for finding center point of mesh
	double total_x = 0;
	double total_y = 0;
	double total_z = 0;
	double total_length = 0;

	// starting edge (take pointer from bounding box vertex)
	Edge* a = vertices[index_of_first_edge]->edge;

	/* 
	We loop through our edges and check if any half edge points to the same vertex, if so, it's an edge.
	When we orientate the meshes the two separate ones will all be clockwise, and therefore the edges will
	be the only place where two half edges point to the same vertex
	*/

	int count = 0;

	do {

		count++;
		Vertex& y = *a->belongs_vertex;
		Vertex& z = *a->pair->belongs_vertex;

		if (y == z)
		{

			edge_pointers.push_back(make_pair(&y, 0));

				//add points values to totals
			total_x += y.returnX();
			total_y += y.returnY();
			total_z += y.returnZ();

		}

		a = a->pair->next;

	} while (a != vertices[index_of_first_edge]->edge);

	cout << "Number of half edges visited: " << count << "." << " Number of edge vertices found is: " << edge_pointers.size() << "." << '\n';
	
	// we can now separate the 'top' and 'bottom' mesh into two by destroying the pairs above so we do not iterate through both

	for (size_t i = 0; i < edge_pointers.size(); i++)
	{

		Vertex& j = *edge_pointers[i].first;

		// doubles for finding average edge length (used for growing fracture)
		const double x1 = j.edge->getP1().returnX();
		const double y1 = j.edge->getP1().returnY();
		const double z1 = j.edge->getP1().returnZ();
		const double x2 = j.edge->getP2().returnX();
		const double y2 = j.edge->getP2().returnY();
		const double z2 = j.edge->getP2().returnZ();

		average_edge_length += std::sqrt(std::pow(x1 - x2, 2) + std::pow(y1 - y2, 2) + std::pow(z1 - z2, 2));

		// delete the edges pair/brother so we do not iterate through the same edges again
		if (j.edge->pair)
		{

			j.edge->pair->pair = nullptr;
			j.edge->pair = nullptr;

		}
	
	}
	
	//create average center point for unit vectors later
	double total = edge_pointers.size();
	Vertex center(total_x/total, total_y/total, total_z/total);
	
	//find average edge length
	average_edge_length = average_edge_length / total;
	average_edge_length = std::sqrt(std::pow(average_edge_length, 2) - std::pow(average_edge_length/2, 2));
	center_point = center;

	//set current outside edges as the edge pointers
	current_edges = edge_pointers;
	previous_edges = edge_pointers;
	all_edges_created = edge_pointers;
	current_extended_nodes = edge_pointers;
	//cout << edge_pointers.size() << '\n';
}

void Mesh::add_Triangle(Vertex* v1, Vertex* v2, Vertex* v3, const bool outside_strip)
{

	//create new objects from the vertices and add them to the vector
	Vertex* p1 = v1;
	Vertex* p2 = v2;
	Vertex* p3 = v3;

	vertices.push_back(p1);
	vertices.push_back(p2);
	vertices.push_back(p3);

	//create a new face and add it to our vector of faces
	Triangle* t = new Triangle(*p1, *p2, *p3);

	/*if (check)
	//{

		std::vector<Vertex> ray1;
		std::vector<Vertex> ray2;
		std::vector<Vertex> ray3;

		ray1.push_back(*p1);
		ray1.push_back(*p2);

		ray2.push_back(*p3);
		ray2.push_back(*p2);

		ray3.push_back(*p3);
		ray3.push_back(*p1);

		for (size_t i = 0; i < other_fracture_triangles.size(); i++)
		{

			Vertex test(0, 0, 0);

			Triangle &n = *other_fracture_triangles[i];

			size_t y = n.intersect_Ray(ray1, test);
			size_t y2 = n.intersect_Ray(ray2, test);
			size_t y3 = n.intersect_Ray(ray3, test);

			if (y != 0 && y != 1 || y2 != 0 && y2 != 1 || y3 != 0 && y3 != 1)
				cout << y << y2 << y3 << '\n';

			if ((y + y2 + y3) == 2)
			{

				delete t;

				return;
			}

		}

	//}*/

	faces.push_back(t);

	//create the edges for the triangle and check if they already exist in our map, if so create brothers
	Edge* e1 = new Edge(*p1, *p2, *p3);
	Edge* e2 = new Edge(*p2, *p3, *p1);
	Edge* e3 = new Edge(*p3, *p1, *p2);

	hedges.push_back(e1);
	hedges.push_back(e2);
	hedges.push_back(e3);

	//create necessary connections
	t->edge = e1;

	//create loop through triangle edges
	e1->next = e2;
	e2->next = e3;
	e3->next = e1;

	//set triangle t as owner of all three edges
	e1->adjacent_triangle = t;
	e2->adjacent_triangle = t;
	e3->adjacent_triangle = t;

	//set pointing vertex to edges
	e1->belongs_vertex = p1;
	e2->belongs_vertex = p2;
	e3->belongs_vertex = p3;

	//set outoging edges to vertices
	p1->edge = e1;
	p2->edge = e2;
	p3->edge = e3;

	//check if any of the edges already exist in mesh, if so, make them brothers so we know the triangles are next to each other
	std::pair<Vertex, Vertex> pair1 = std::make_pair(std::min(*p1, *p2), std::max(*p1, *p2));
	std::pair<Vertex, Vertex> pair2 = std::make_pair(std::min(*p2, *p3), std::max(*p2, *p3));
	std::pair<Vertex, Vertex> pair3 = std::make_pair(std::min(*p1, *p3), std::max(*p1, *p3));

	map< pair<Vertex, Vertex>, Edge* >::iterator find = half_edges.find(pair1);
	map< pair<Vertex, Vertex>, Edge* >::iterator find2 = half_edges.find(pair2);
	map< pair<Vertex, Vertex>, Edge* >::iterator find3 = half_edges.find(pair3);

	// find pairs of half edges (same edge, but bounded by different faces) - we check if this is the outside triangle
	// if so [outside_strip], then it is necessary NOT to create a brother to the other side of the mesh as this is the border edge
	if (find != half_edges.end() && !outside_strip)
	{
		Edge &y = *(find->second);
		e1->pair = &y;
		y.pair = e1;

	}
	else {
		//cout << "Found pair!" << '\n';
		if (!outside_strip)
		{
			half_edges.insert(std::make_pair(pair1, e1));
		}

	}

	if (find2 != half_edges.end())
	{

		Edge &y = *(find2->second);
		e2->pair = &y;
		y.pair = e2;

	}
	else {
		//cout << "Found pair!" << '\n';
		half_edges.insert(std::make_pair(pair2, e2));

	}

	if (find3 != half_edges.end())
	{

		Edge &y = *(find3->second);
		e3->pair = &y;
		y.pair = e3;

	}
	else {
		//cout << "Found pair!" << '\n';
		half_edges.insert(std::make_pair(pair3, e3));

	}

}

void Mesh::grow_Fractures(const float z_change_percent)
{

	std::vector<pair<Vertex*, int> > new_points;

	double x = center_point.returnX();
	double y = center_point.returnY();
	double z = center_point.returnZ();
	
	double change_z = 1 + (z_change_percent / 100);

	//extend current nodes once again to use to create new growth points
	for (size_t i = 0; i < current_extended_nodes.size(); i++)
	{
		Vertex &t = *current_extended_nodes[i].first;

		const double current_z = t.returnZ();
		const double z1 = edge_pointers[i].first->returnZ();
		 
		const double x1 = t.returnX();
		const double y1 = t.returnY();

		//store unit vectors if not yet created
		if (unit_vectors_x.size() <= i || unit_vectors_y.size() <= i || unit_vectors_z.size() <= i)
		{

			double unit_vector_x = x1 - x;
			double unit_vector_y = y1 - y;
			double unit_vector_z = z1*change_z - z;

			double magnitude = std::sqrt(std::pow(unit_vector_x, 2) + std::pow(unit_vector_y, 2) + std::pow(unit_vector_z, 2));

			unit_vector_x = unit_vector_x / magnitude;
			unit_vector_y = unit_vector_y / magnitude;
			unit_vector_z = unit_vector_z / magnitude;

			unit_vectors_x.push_back(unit_vector_x);
			unit_vectors_y.push_back(unit_vector_y);
			unit_vectors_z.push_back(unit_vector_z);

		}



		//create the new point from unit vectors above
		Vertex *newp = new Vertex(x1 + unit_vectors_x[i] * average_edge_length, y1 + unit_vectors_y[i] * average_edge_length, current_z + unit_vectors_z[i] * average_edge_length);
			
		new_points.push_back(make_pair(newp, 0));

	}

	//after extending the nodes once, we can delete the extended node objects from before as they are no longer needed
	//we only need to remember the latest set
	if (current_edges != current_extended_nodes)
	{
		for (size_t i = 0; i < current_extended_nodes.size(); i++)
		{

			delete current_extended_nodes[i].first;
			
		}
	}

	//make the new points the latest current extended nodes
	current_extended_nodes = new_points;

	//create triangle objects from the new set of points
	create_Triangles(other_fractures);

	// clear the holder so we can grow more strips
	new_points.clear();

}

bool Mesh::create_Triangles(std::vector<Mesh*> other_fractures)
{

	std::vector<std::pair<Vertex*, int> > holder;
	size_t cut_off = rock.size() - other_fracture_triangles.size();

	for (size_t i = 0; i < current_extended_nodes.size(); i++)
	{
		//snapped boolean to determine if we've created the triangles before
		bool snapped = false;

		//2 extended node vertices
		Vertex &extended_point = *current_extended_nodes[i].first;
		Vertex &next_extended_point = *current_extended_nodes[(i + 1) % current_extended_nodes.size()].first;

		//2 old edge vertices from vector
		auto &current_edge_vertex = current_edges[i]; // current point
		auto &next_edge_vertex = current_edges[(i + 1) % current_edges.size()]; //next point
		auto &previous_edge_vertex = current_edges[((i + current_edges.size() - 1) % current_edges.size())]; //previous point

		//create new point which is between the two points to preserve equilateral triangle nature
		double newx = 0;
		double newy = 0;
		double newz = 0;

		//create a ray for intersection
		std::vector<Vertex> ray;

		//we check if the points (current extended nodes) are the ones we need for creating triangles or if we need the midpoint
		if (!onextended)
		{
			newx = (extended_point.returnX() + next_extended_point.returnX()) / 2;
			newy = (extended_point.returnY() + next_extended_point.returnY()) / 2;
			newz = (extended_point.returnZ() + next_extended_point.returnZ()) / 2;

			/*cout << newx << '\n';

			const double old_x = (g.returnX() + g2.returnX()) / 2;
			const double old_y = (g.returnY() + g2.returnY()) / 2;

			double uvx = old_x - newx;
			double uvy = old_y - newy;

			const double mag = std::sqrt(std::pow(uvx,2) + std::pow(uvy,2));

			uvx = uvx / mag;
			uvy = uvy / mag;

			const double difference = average_edge_length - mag;

			newx = newx + uvx * difference;
			newy = newy + uvy * difference;
			cout << newx << '\n';*/
		}
		else
		{

			newx = extended_point.returnX();
			newy = extended_point.returnY();
			newz = extended_point.returnZ();

		}

		Vertex *newp = new Vertex(newx, newy, newz);

		//create 'ray' to check for intersection
		if (previous_edges.empty())
			ray.push_back(center_point);
		else {
			ray.push_back(*previous_edges[i].first);
		}

		ray.push_back(*newp);

		snapped = (previous_edges[i].second != 0); //check pair bool value [snapped?] to determine if we need to proceed
		int point_Snapped = previous_edges[i].second; //for creating new pair of (point, bool isSnapped)
		bool snapped_Fracture = false; //bool for checking if any of our points are snapped to the fracture

		//check intersection and also if we have snapped the edge to the wall already or not
		for (size_t t = 0; t < rock.size(); t++ && !snapped)
		{

			Triangle &border = *rock[t];
			Vertex test(0, 0, 0);

			size_t g = border.intersect_Ray(ray, test);

			//intersection 
			if (g == 1)
			{
				// set the created point as intersection point and set bool isSnapped to true
				*newp = test;
				if (t < cut_off)
					point_Snapped = 1;
				else
					point_Snapped = 2;

			}
		}

		if (snapped)
		{
			delete newp;
			holder.push_back(previous_edges[i]);
		}
		else
		{
			holder.push_back(make_pair(newp, point_Snapped));
		}

		//check snapped status of points going to be used for creation of triangles
		const int c_edge_snap = current_edge_vertex.second;
		const int p_edge_snap = previous_edge_vertex.second;
		const int n_edge_snap = next_edge_vertex.second;

		//create a sum of how many of the points are snapped to stop intersection of new triangles with fracture
		if (onextended)
			snapped_Fracture = (c_edge_snap == 2 || p_edge_snap == 2) && point_Snapped != 2;
		else
			snapped_Fracture = (c_edge_snap == 2 || n_edge_snap == 2) && point_Snapped != 2;

		/*if (onextended && c_edge_snap > 0 && point_Snapped > 0 && p_edge_snap > 0)
			snapped = true;
		else
			if (c_edge_snap > 0 && point_Snapped > 0 && n_edge_snap > 0)
				snapped = true;*/

		if (!snapped)
		{
			//add to necessary containers and create the first triangle strip
			all_edges_created.push_back(make_pair(newp,point_Snapped));

			if (!snapped_Fracture)
			{
				if (onextended)
					add_Triangle(current_edge_vertex.first, newp, previous_edge_vertex.first, false);
				else
					add_Triangle(current_edge_vertex.first, newp, next_edge_vertex.first, false);
			}

			//create the other side of the triangle strip
			if (holder.size() > 1)
			{
				
				if (onextended)
					snapped_Fracture = (holder[i - 1].second == 2 || p_edge_snap == 2) && point_Snapped != 2;
				else
					snapped_Fracture = (holder[i - 1].second == 2 || c_edge_snap == 2) && point_Snapped != 2;

				if (!snapped_Fracture)
				{
					if (onextended)
						add_Triangle(holder[i - 1].first, newp, previous_edge_vertex.first, true);
					else
						add_Triangle(holder[i - 1].first, newp, current_edge_vertex.first, true);
				}

				if (i == current_extended_nodes.size() - 1)
				{
					if (onextended)
						add_Triangle(newp, holder[0].first, current_edges[current_edges.size() - 1].first, true);
					else
						add_Triangle(newp, holder[0].first, current_edges[0].first, true);

				}

			}

		}

	}

	// set new container holders and reverse extended boolean for next node generation
	previous_edges = current_edges;
	current_edges = holder;
	onextended = !onextended;
	return holder.size() == current_extended_nodes.size();
}

void Mesh::grow_For(size_t i, const float angle, const bool alternating, std::vector<Mesh*> other_fracs)
{

	//set our other fracture variable equal to the fractures inputted
	other_fractures = other_fracs;

	//create a vector with all the other triangles which we have to SNAP to (rock wall, if given + other fractures)
	for (size_t y = 0; y < other_fractures.size(); y++)
	{

		std::vector<Triangle*> &other = other_fractures[y]->return_Faces();

		other_fracture_triangles.reserve(other_fracture_triangles.size() + other.size());
		other_fracture_triangles.insert(other_fracture_triangles.end(), other.begin(), other.end());

	}

	//add the other fracture faces into our limiting growth ('rock') holder
	rock.reserve(other_fracture_triangles.size() + rock.size());
	rock.insert(rock.end(), other_fracture_triangles.begin(), other_fracture_triangles.end());

	//cout << rock.size() << '\n';

	// check if we are making an up/down fracture
	if (!alternating)
	{
		for (size_t t = 0; t < i; t++)
		{

				grow_Fractures(angle);
				cout << faces.size() << '\n';

		}
	}
	else {
		//grow up and down
		for (size_t y = 0; y < i; y++)
		{
			if (y == 0)
			{

				grow_Fractures(angle);

			}

			else

			{

				if (onextended)
					grow_Fractures(-2*angle);
				else
					grow_Fractures(2*angle);

			}

		}

	}

}

std::vector<Triangle*> Mesh::return_Faces()
{

	return faces;

}

void Mesh::simplify_Mesh()
{

	//holder for each plane to store the MIN/MAX pair of the planes points
	vector< pair<Vertex, Vertex> > temp;
	int count = 0;

	for (map <pair<Vertex, Vertex>, vector<Triangle*> >::iterator it = simple_rock_mesh.begin(); it != simple_rock_mesh.end(); ++it)
	{

		if (temp.size() <= count)
		{

			Vertex start1(0, 0, 0);
			Vertex start2(0, 0, 0);

			temp.push_back(make_pair(start1, start2));

		}

		vector<Triangle*> &holder = it->second;

		for (size_t i = 0; i < holder.size(); i++)
		{

			Triangle &t = *holder[i];

			Vertex min = temp[count].first;
			Vertex max = temp[count].second;

			Vertex &p1 = t.getP1();
			Vertex &p2 = t.getP2();
			Vertex &p3 = t.getP3();

			//if (p1 < )


		}

	}

}