using namespace std;
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include "../Headers/Vertex.h"
#include "../Headers/Triangle.h"
#include "../Headers/Edge.h"
#include "../Headers/Mesh.h"
#include <ctime>
#include <map>
#include <set>

/*

16/06/2015
Jan Lietava

*/

int main()
{
	
	clock_t begin = clock();

	// initiate fracture objects from file
	Mesh* nodes116one = new Mesh("Meshes/mesh_116_4721.stl", "", "FRACTUREONE");
	Mesh* nodes116two = new Mesh("Meshes/mesh_116_4721.stl", "", "FRACTURETWO");
	Mesh* nodes116three = new Mesh("Meshes/mesh_116_4721.stl", "", "FRACTURETHREE");
	Mesh* nodes4721 = new Mesh("Meshes/mesh_116_4721.stl", "ROCK", "FRACTUREFOUR");

	cout << "Now to work out the border edges..." << '\n';
	
	//fine outside egdes for the one we want to grow
	
	nodes4721->find_Edges_Pointers();
	//nodes116one->find_Edges_Pointers();

	//create vector of other fractures
	std::vector<Mesh*> fractures;

	//try growing one small fracture first
	//nodes116one->grow_For(2, 0, false, fractures);

	fractures.push_back(nodes116one);
	fractures.push_back(nodes116two);
	fractures.push_back(nodes116three);

	//std::vector<Mesh*> fractures;
	nodes4721->grow_For(0, 0, false, fractures);

	//nodes4721->vertices_into_File();
	nodes4721->create_rhino_File("Meshes/Output/testnew.stl");

	clock_t end = clock();
	double time_taken = double(end - begin) / CLOCKS_PER_SEC;
	cout << "Time taken: " << time_taken << "." << '\n';

	delete nodes4721;
	//delete nodes116;

	return 0;
}