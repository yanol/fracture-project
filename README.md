# README #

##Description##

The goal of this program is to load and manage fractures in the .stl format using the half-edge data structure concept. It allows the user to grow fractures using several different methods, and therefore can be used to model multiple fracture growth and interraction within a limited space (a rock for example).

##How to use this program:##

* Just using the main.cpp file, it is possible to change the possible fracture interactions. 
* Load a fracture by initialising through the Mesh() class.
* After this, the edge pointers must be explicitly found using find_Edges_Pointers().
* Finally, like in the example main.cpp included in this source, the grow() methods can be used to add to the mesh.
* After this, the mesh can be saved, and the .stl file (in the Output folder) can be used through any CAD software.
* There are several fractures included which can be used to test. More information about the possibilities is included below.

Capability:

* Initialise .stl mesh file (load triangle data, orientation, store vertices and edges in a map, create links between edges)
* Find edges of the mesh, as well as the vertices. 
* Export and store vertices, or output .stl file at any point.
* Add triangles to the outer edges.
* Grow the 'fracture' (mesh) outwards, either in a same plane as the original mesh, or at an angle. It is also possible to have alternating growth to model the zig-zag effect of fractures (still using any angle input).
* Return faces of the mesh

##How do I get set up?##

* This program was committed directly to git from visual studio, so using the same version (2015) it is easily possible to set up. Otherwise, the source C++ and header files can be used to import into the chosen IDE.
* After setting up, check the mesh folder to determine which ones to use, or import your own. Currently only supports .stl format.

##Contributors:##

Jan Lietava

### Who do I talk to? ###

* Jan Lietava (lietava.jan@gmail.com)

##License##

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
