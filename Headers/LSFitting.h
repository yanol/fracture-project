#pragma once
#include "Vertex.h"
#include <vector>

class LSFitting
{

	double a_0, a_1, a_2;

public:
	LSFitting(std::vector<Vertex*> in);
	~LSFitting();

private:
	//sums for least squares fitting
	double sum_x = 0;
	double sum_x2 = 0;
	double sum_x3 = 0;
	double sum_x4 = 0;

	double sum_y = 0;
	double sum_xy = 0;
	double sum_x2y = 0;

	std::vector<double> matrix_1;
	std::vector<double> matrix_2;

	bool generate_Matrix(std::vector<Vertex*> in);
	bool invert_Matrix(std::vector<double>& in);
	bool generate_Constants();
};

