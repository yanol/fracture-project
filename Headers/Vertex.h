#pragma once
using namespace std;
#include <math.h>

class Edge;

class Vertex
{

	double x, y, z;

public:
	//edge that comes out of the vertex
	Edge *edge = nullptr;
	Vertex(double x1, double y1, double z1);
	Vertex();
	~Vertex();
	void print_Vertexs();
	double returnX() const;
	double returnY() const;
	double returnZ() const;
	Vertex operator+(const Vertex &rhs)
	{

		return Vertex(this->x + rhs.returnX(), this->y + rhs.returnY(), this->z + rhs.returnZ());

	}
	Vertex operator*(const double rhs)
	{

		return Vertex(this->x*rhs, this->y*rhs, this->z*rhs);

	}
	Vertex operator/(const double rhs)
	{

		return Vertex(this->x/rhs, this->y/rhs, this->z/rhs);

	}
	Vertex operator-(const Vertex &rhs)
	{

		return Vertex(this->x - rhs.returnX(), this->y - rhs.returnY(), this->z - rhs.returnZ());

	}
	bool operator()(Vertex* test)
	{

		return fabs(test->returnX() - x) < 0.00001
			&& fabs(test->returnY() - y) < 0.00001;

	}
	bool operator==(const Vertex &rhs) const
	{

		return fabs(this->x - rhs.returnX()) < 0.00001
			&& fabs(this->y - rhs.returnY()) < 0.00001
			&& fabs(this->z  - rhs.returnZ()) < 0.00001;

	}
	bool operator<(const Vertex &b) const
	{

		return this->x < b.returnX() || (this->x == b.returnX() && this->y < b.returnY()) || (this->x == b.returnX() && this->y == b.returnY() && this->z < b.returnZ());

	}
};

