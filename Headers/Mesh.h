#pragma once
#include "Edge.h"
#include "Vertex.h"
#include "Triangle.h"
#include <vector>
#include <string>
#include <set>
#include <list>
#include <map>

class Mesh
{

	std::vector<Edge*> hedges;
	std::vector<Triangle*> faces;
	std::vector<Vertex*> vertices;


public:
	Mesh(const std::string inputfile, const string wall_name, const string fracture_name); //initiates a mesh object from .stl file, where the wall_name is the name of the wall object in the .stl and fracture_name is the name of the fracture
	void vertices_into_File(); //creates two files of x and y coords to plot the boundary of the fracture
	void find_Edges_Pointers(); //uses the original bounding box vertex and finds all the other edge vertices (as pointers)
	void add_Triangle(Vertex* v1, Vertex* v2, Vertex* v3, const bool outside_strip); // adds triangle into the mesh
	void grow_Fractures(const float z_change_percent); //generates new edge points for new mesh triangles, where angle denotes if it stays level, and growth level suggests how many triangles are added
	void grow_For(size_t i, const float angle, const bool alternating, std::vector<Mesh*> other_fractures); //grows for amounts of i
	void create_rhino_File(const string fileName);
	std::vector<Triangle*> return_Faces();
	void simplify_Mesh(); //simplifies the rock mesh into triangles which have the same plane normal

	~Mesh();

private:

	//VARIABLES

	std::vector<Triangle*> rock; //vector of triangles of the rock/bounding object from file
	std::vector<Mesh*> other_fractures; //vector of other fracture meshes interracting with this mesh
	std::vector<Triangle*> other_fracture_triangles; //vector which stores mesh of triangles of other fractures

	bool onextended = false; //bool which remembers if we are creating edge vertices on the same unit vector as original edges

	double average_edge_length; //average length of edge - used when creating new triangles

	//pairs in the form of (vertex,int) where the int is 0 (not snapped), 1 (snapped to wall), 2 (snapped to fracture)
	std::vector<std::pair<Vertex*, int> > all_edges_created; // stores all the new edge vertices created each time the grow function is ran
	std::vector<std::pair<Vertex*, int> > edge_pointers; // stores the original pointers to the original vertices of the fracture
	std::vector<std::pair<Vertex*, int> > current_edges; // stores the current outside edge vertices (AVERAGED out for equilateral nature)
	std::vector<std::pair<Vertex*, int> > previous_edges; // stores the previous edges for use in growing
	std::vector<std::pair<Vertex*, int> > current_extended_nodes; //stores the direct NON-equilateral extended edge points - see CREATE triangles method.

	std::vector<double> unit_vectors_x; //vector which stores all the unit vectors, saving time
	std::vector<double> unit_vectors_y; //vector which stores all the unit vectors, saving time
	std::vector<double> unit_vectors_z; //vector which stores all the z unit vectors

	Vertex center_point; //the center of the mesh

	std::map< pair<Vertex, Vertex>, Edge* > half_edges; //map of brother half edges
	std::map< pair<Vertex, Vertex>, vector<Triangle*> > simple_rock_mesh; //map to check if any of the bounding rock triangles
	//can be simplified (by checking their unit normal [Vertex])
	
	//bounding box vertex pointer to start tip growing
	Vertex first_edge_Vertex; //temp object that is the first edge vertex
	int index_of_first_edge = 0; //index to the pointer of the first vertex point
	Vertex* ptr_to_first_vertex; //pointer to the first edge vertex within our object holders

	//FUNCTIONS

	//initial method to create vectors of pointers which define the mesh - creates links between the objects
	bool create_Vertices_and_Edges(std::string inputfile, const string wall_name, const string fracture_name);
	bool create_Triangles(std::vector<Mesh*> other_fractures); //creates new strip of triangles from extended nodes - bool is to determine whether we have to average out the points or not (equilateral identity)
};

