#include "Vertex.h"
#include <vector>

class Edge;

#pragma once
class Triangle
{

	Vertex p1,p2,p3;

public:
	// half edge bordering the triangle
	Edge *edge = nullptr;
	Triangle(Vertex v1, Vertex v2, Vertex v3)
		: p1(v1), p2(v2), p3(v3) {};
	Triangle();
	~Triangle();
	pair<Vertex, Vertex> surface_normal();
	
	void print_vertices();

	bool overlap(Triangle& h);
	int intersect_Ray(std::vector<Vertex> &ray, Vertex &intersect);

	Vertex getP1();
	Vertex getP2();
	Vertex getP3();

private:
	bool same_Side(const Vertex& a, const Vertex& b, const Vertex& c);
	Vertex cross_Product(Vertex &a, Vertex &b);
	double dot_Product(Vertex &a, Vertex &b);

};

