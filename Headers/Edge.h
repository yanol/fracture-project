#pragma once
#include "Vertex.h"
#include <math.h>
using namespace std;

class Triangle;

class Edge
{

	Vertex p1, p2;
	Vertex outside1;

public:

	Triangle *adjacent_triangle = nullptr; //triangle it belongs to
	Edge *pair = nullptr; // the half edge going the other way
	Edge *next = nullptr; // the next half edge in the triangle
	Vertex *belongs_vertex = nullptr; //vertex the egde points to

	Edge(Vertex v1, Vertex v2, Vertex t)
		: p1(v1), p2(v2), outside1(t) {};
	Edge();
	~Edge();
	Vertex getP1() const;
	Vertex getP2() const;
	Vertex getNonEdgeVertex() const;
	bool operator==(const Edge& rhs) const
	{
		return
			p1 == rhs.getP1()
			&& p2 == rhs.getP2()

			||

			p1 == rhs.getP2()
			&& p2 == rhs.getP1();

	}
	/*bool operator<(const Edge& rhs) const
	{

		return p1 < rhs.getP1() && p2 < rhs.getP2() || p1 == rhs.getP1() && p2 < rhs.getP2() || p2 == rhs.getP2() && p1 < rhs.getP1()
			|| p1 == rhs.getP2() && p2 < rhs.getP1() || p2 == rhs.getP1() && p1 < rhs.getP2();

	}*/
};
